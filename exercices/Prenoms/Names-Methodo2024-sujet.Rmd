---
title: "French given names per year per department"
author: "Lucas Mello Schnorr, Jean-Marc Vincent"
date: "October, 2022"
output:
  pdf_document: default
  html_document:
    df_print: paged
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
# Contexte du problème
L'objectif de cette activité est de développer une méthode pour répondre à des questions à partir d'un jeu de données fourni.

Le jeu de données sera le fichier des prénoms établi par l'INSEE, car les question peuvent être graduelles de la fréquence d'un prénom à la répartition géographique des prénoms, jusqu'à des mesures d'homogénéité territoriales.

SOURCE: [https://www.insee.fr/fr/statistiques/fichier/7633685/dpt2022_csv.zip](https://www.insee.fr/fr/statistiques/fichier/7633685/dpt2022_csv.zip), 

Ce jeu de données a été choisi également car il a une structure simple et qu'il est suffisemment grand pour que le travail ne puisse se faire à la main.

On aura besoin de la bibliothèque _tidyverse_ pour ces analyses. 
Décompresser  _dpt2020_txt.zip_ pour obtenir **dpt2022.csv**). Lire ce fichier en R, (éventuellement charger le package  `readr).

## Download Raw Data from the website
```{r}
file = "dpt2022_txt.zip"
if(!file.exists(file)){
  download.file("https://www.insee.fr/fr/statistiques/fichier/7633685/dpt2022_csv.zip",
	destfile=file)
}
unzip(file)
```
Check if your file is the same as in the first analysis (reproducibility)
```{bash}
md5 dpt2022.csv
```
expected :
MD5 (dpt2022.csv) = 51aa7a3cd0ff01fe07b35816c83a0138

## Build the Dataframe from file

```{r}
library(tidyverse)

##FirstNames <- read_delim("dpt2022.csv",delim=";")

FirstNames <- read_delim(("dpt2022.csv"), delim=";", col_types= "fciii", na=c("XX","XXXX"))
FirstNames%>% tail(10)
# Définir le prénom à rechercher
prenom <- "ROSARIO"

# Filtrer les données pour obtenir toutes les lignes contenant le prénom choisi
donnees_rosario <- FirstNames %>%
  filter(preusuel == prenom)

# Afficher les données
donnees_rosario



```

Toutes les questions suivantes peuvent nécessiter des analyses préliminaires, ne pas hésiter à présenter votre travail de manière judicieuse en explicitant votre démarche d'analyse et rédiger le compte rendu comme un rapport scientifique. 

Questions: 
1. Choisir un prénom et analyser sa fréquence au cours du temps, comparer avec d'autres prénoms.
2. Donner, par genre, le prénom le plus attribué par année. Analyser l'évolution du prénom le plus fréquent.
3. Quels départements ont la plus forte variété de de prénoms ? 


